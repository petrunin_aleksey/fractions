package ru.pae.fractions;

public class Rational {
    private int num;
    private int den;

    Rational(int num, int den) {
        this.num = num;
        this.den = den;
    }

    private int getNum() {
        return num;
    }

    private int getDen() {
        return den;
    }
    Rational mult(Rational rational) {
        int num = this.num * rational.num;
        int den = this.den * rational.den;
        return new Rational(num, den).sokr();

    }
    Rational div(Rational rational) {
        int num = this.num * rational.den;
        int den = this.den * rational.num;
        return new Rational(num, den).sokr();
    }
    Rational add(Rational rational) {
        int num;
        int den;
        if (this.den == rational.den) {
            num = this.num + rational.num;
            den = this.den;
        } else {
            num = this.num * rational.den + rational.num * this.den;
            den = this.den * rational.den;
        }
        return new Rational(num, den).sokr();
    }
    Rational sub(Rational rational) {
        int num;
        int den;
        if (this.den == rational.den) {
            num = this.num - rational.num;
            den = this.den;
        } else {
            num = this.num * rational.den - rational.num * this.den;
            den = this.den * rational.den;
        }
        return new Rational(num, den).sokr();
    }

    private Rational sokr() {
        int a = this.num;
        int b = this.den;
        int num1 = this.num / nod(a,b);
        int den1 = this.den / nod(a,b);
        return new Rational(num1, den1);
    }

    private int nod(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    @Override
    public String toString() {
        return num +
                "/" + den;
    }
}
